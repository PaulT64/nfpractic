package InsuranceApp.InsuranceTypes;

import java.util.List;

public class DeferredLifeLongInsuranceForNyears extends Type {


    public DeferredLifeLongInsuranceForNyears(String name, int age, int limitAge, double percent, double contractSum, double premiya, int n, List<double[]> rowList){
        super(name, age, limitAge, percent, contractSum, premiya, n, rowList);
    }
    @Override
    public double getNettoPremiya() {
        double A;
        double Sigma =Math.log(1+Percent/100);

        A = (Math.pow(1.0/(1.0 + Percent/100),N) - Math.pow(1.0/(1.0 + Percent/100),(LimitAge-Age)))/((LimitAge - Age)*Sigma);
        return A;
    }
}
