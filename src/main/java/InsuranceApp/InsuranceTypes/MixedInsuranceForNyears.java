package InsuranceApp.InsuranceTypes;

import java.util.List;

public class MixedInsuranceForNyears extends Type {

    public MixedInsuranceForNyears(String name, int age, int limitAge, double percent, double contractSum, double premiya, int n, List<double[]> rowList){
        super(name, age, limitAge, percent, contractSum, premiya, n, rowList);
    }

    @Override
    public double getNettoPremiya() {
        double sum = 0;
        double sigma;
        sigma = Math.log(1 + (Percent / 100));
        for(int i=0; i<N; i++){
            sum += Math.pow(1.0/(1.0 + Percent/100), i+1)*(Percent/100/sigma)*(rowList.get(Age)[5])/(rowList.get(Age)[4]);
        }
        double A = Math.pow(1.0/(1.0 + Percent/100),N)*(rowList.get(Age+N)[4])/(rowList.get(Age)[4]);
        return A;
    }
}
