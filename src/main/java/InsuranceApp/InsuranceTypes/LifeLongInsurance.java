package InsuranceApp.InsuranceTypes;

import java.util.List;

public class LifeLongInsurance extends Type {


    public LifeLongInsurance(String name, int age, int limitAge, double percent, double contractSum, double premiya, int n, List<double[]> rowList){
        super(name, age, limitAge, percent, contractSum, premiya, n, rowList);
    }


    @Override
    public double getNettoPremiya() {
        double sigma;
        double A;
        sigma = Math.log(1 + (Percent / 100));
        A = (1 - Math.exp(- sigma * (LimitAge - Age)))/(sigma * (LimitAge - Age));
        return A;
    }
}
