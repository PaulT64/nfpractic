package InsuranceApp.InsuranceTypes;
import java.util.List;

public class AccumulativeInsuranceForNyears extends Type {
    private double Px;

    public AccumulativeInsuranceForNyears(String name, int age, int limitAge, double percent, double contractSum, double premiya, int n, List<double[]> rowList){
       super(name, age, limitAge, percent, contractSum, premiya, n, rowList);
    }

    @Override
    public double getNettoPremiya(){
        double A;
        A = Math.pow(1.0/(1.0 + Percent/100),N)*(1 - rowList.get(Age)[2]);
        return A;
    }

}
