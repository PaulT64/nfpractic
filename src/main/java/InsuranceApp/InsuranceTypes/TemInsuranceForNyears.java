package InsuranceApp.InsuranceTypes;

import java.util.List;

public class TemInsuranceForNyears extends Type {


    public TemInsuranceForNyears(String name, int age, int limitAge, double percent, double contractSum, double premiya, int n, List<double[]> rowList){
        super(name, age, limitAge, percent, contractSum, premiya, n, rowList);
    }

    @Override
    public double getNettoPremiya() {
        double sigma = Math.log(1 + (Percent / 100));
        double A = (1 - Math.pow(1.0/(1.0 + Percent/100),LimitAge - Age))/((LimitAge - Age)*sigma);
        return A;
    }
}
