package InsuranceApp.InsuranceTypes;

import java.util.ArrayList;
import java.util.List;

public abstract class Type {

    protected String Name; //название модели
    protected int Age; //возраст застрахованного
    protected int LimitAge; //максимально возможный возраст жизни
    protected double Percent; //процентная ставка
    protected double ContractSum; //сумма, на которую заключили договор
    protected double Premiya; //страховая премия
    protected double NettoPremiya; //нетто-премия
    protected int N;
    protected List<double[]> rowList;

    public Type(String name, int age, int limitAge, double percent, double contractSum, double premiya, int n, List<double[]> rowList){
        this.Name = name;
        this.Age = age;
        this.LimitAge = limitAge;
        this.Percent = percent;
        this.ContractSum = contractSum;
        this.Premiya = premiya;
        this.N = n;
        this.rowList = rowList;
    }

    public double getChance() {
        double sigma = Math.log(1 + (Percent / 100));
        double Chance = (Math.log(ContractSum/Premiya))/((LimitAge - Age)*sigma);
        return Chance; //вероятность того, что договор будет убыточным

    }

    public double getNettoPremiya() {
        return NettoPremiya;
    }
}
