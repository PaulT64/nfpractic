package InsuranceApp.CalculationHandler;

import InsuranceApp.InsuranceTypes.LifeLongInsurance;
import InsuranceApp.InsuranceTypes.Type;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class Handler {

    private String Name;
    private double chance;
    private final BlockingQueue<Type> queue1 = new ArrayBlockingQueue<>(5);
    private final String[] STR = new String[5];
    private List<double[]> rowList = new ArrayList<>();
    private Map values1 = new HashMap<String,Double>();
    private Map values2 = new HashMap<String,Double>();

    public Handler(String name){
        this.Name = name;
    }

    public void Start(int Age,int LimitAge,double Percent,double ContractSum,double Premiya,int N,int GENDER) throws FileNotFoundException { //вычисляет нетто-премии по всем договорам в очереди и записывает их в массив
        HandlerHelper HELPER = new HandlerHelper("HELPER");
        WorkWithFiles WK = new WorkWithFiles("WORKER");
        ClassLoader classLoader = getClass().getClassLoader();

        switch (GENDER){
            case 1:
                InputStream in = getClass().getResourceAsStream("/ExcelFiles/DeathMen.xls");
                try {
                    WK.Work(in);
                    rowList = WK.getRowList();  //передача числовых значений из файла в этот класс

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case 2:
                InputStream in1 = getClass().getResourceAsStream("/ExcelFiles/DeathWomen.xls");
                    try {
                        WK.Work(in1);
                        rowList = WK.getRowList();  //передача числовых значений из файла в этот класс

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
            default: break;
        }

        for(int i = 0;i < 5; i++){
            try {
                queue1.put(HELPER.Creator(i+1,Age,LimitAge,Percent,ContractSum,Premiya,N,rowList)); //заполнение очереди
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        STR[0] = "Накопительное ";
        STR[1] = "Отложенное ";
        STR[2] = "Пожизненное ";
        STR[3] = "Смешанное ";
        STR[4] = "Временное ";

        int t = 0;
        while (!queue1.isEmpty()) {
            try {
                values1.put(STR[t], queue1.poll().getNettoPremiya() * ContractSum);
                t++;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        LifeLongInsurance lf = new LifeLongInsurance("AAA",Age,LimitAge,Percent, ContractSum, Premiya, N, rowList);
        chance = lf.getChance();
        values2 = MapUtilite.sortByValue(values1);
    }

    public double getChance(){
        return chance;
    }

    public Map getMap(){
        return values2 ;
    }

}
