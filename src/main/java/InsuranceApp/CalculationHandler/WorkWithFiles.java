package InsuranceApp.CalculationHandler;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class WorkWithFiles {
    private String Name;
    List<double[]> rowList = new ArrayList<>(); //лист куда перезаписывается инфа из файла


    public WorkWithFiles(String name){
        this.Name = name;
    }

        public void Work(InputStream file) throws IOException {

        Workbook WB = new HSSFWorkbook(file);

        for(int i=1; i<111; i++){
            try {   double Values[] = new double[6];
                for (int j = 0; j < 6; j++) {
                    Values[j] = WB.getSheetAt(0).getRow(i).getCell(j+1).getNumericCellValue();
                }
                rowList.add(Values);
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    public List<double[]>getRowList(){
        return rowList;
    }

}
