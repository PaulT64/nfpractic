package InsuranceApp.CalculationHandler;

import InsuranceApp.InsuranceTypes.*;

import java.util.List;

class HandlerHelper { //помошник обработчика
                      //создает экземпляры классов из моделей страхования в зависимости от номера модели
                      //и возвращает этот экземпляр

    private String Name;

    HandlerHelper(String name){
        this.Name = name;
    }

    Type Creator(int i, int age, int limitAge, double percent, double contractSum, double premiya,int N, List<double[]> rowList){
        switch (i){
            case 1:
                return new AccumulativeInsuranceForNyears("inst1",age,limitAge,percent,contractSum,premiya,N,rowList);
            case 2:
                return new DeferredLifeLongInsuranceForNyears("inst2",age,limitAge,percent,contractSum,premiya,N,rowList);
            case 3:
                return new LifeLongInsurance("inst3",age,limitAge,percent,contractSum,premiya,N,rowList);
            case 4:
                return new MixedInsuranceForNyears("inst4",age,limitAge,percent,contractSum,premiya,N,rowList);
            case 5:
                return new TemInsuranceForNyears("inst5",age,limitAge,percent,contractSum,premiya,N,rowList);
            default:
                return null;
        }


    }

}
