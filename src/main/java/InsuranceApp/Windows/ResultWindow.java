package InsuranceApp.Windows;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

public class ResultWindow {
    public static void display(String title, Map map, int N, double Chance){
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(400);
        window.setMinHeight(150);

        String[] STR1 = new String[map.size()];
        String[] STR2 = new String[map.size()];

        int i=0;

        for (Object key : map.keySet()) {
            STR1[i] = (String) key;i++;
        }
        i=0;
        for (Object val : map.values()) {
            BigDecimal bd = new BigDecimal((Double) val).setScale(4, RoundingMode.HALF_EVEN);
            val = bd.doubleValue();
            STR2[i] = String.valueOf(val); i++;
        }

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(40,20,40,20));
        gridPane.setVgap(5); gridPane.setHgap(10);

        BigDecimal bd1 = new BigDecimal((Double) Chance).setScale(2, RoundingMode.HALF_EVEN);
        Chance = bd1.doubleValue() * 100;

        Button closeButton = new Button();
        closeButton.setText("Ок");
        closeButton.setTextAlignment(TextAlignment.CENTER);
        closeButton.setMinSize(100,25);


        Label lab1 = new Label("Название модели :"); Label lab2 = new Label("Нетто-премия :");

        Label l1 = new Label(STR1[0]); Label g1 = new Label(STR2[0]);
        Label l2 = new Label(STR1[1]); Label g2 = new Label(STR2[1]);
        Label l3 = new Label(STR1[2]); Label g3 = new Label(STR2[2]);
        Label l4 = new Label(STR1[3]); Label g4 = new Label(STR2[3]);
        Label l5 = new Label(STR1[4]); Label g5 = new Label(STR2[4]);
        Label Result1 = new Label("Вероятность убыточности :"); Label Result2 = new Label(String.valueOf(Chance) + " %");

        GridPane.setConstraints( lab1, 0 ,0);GridPane.setConstraints( lab2, 2 ,0);
        GridPane.setConstraints( l1, 0 ,1);GridPane.setConstraints( g1, 2 ,1);
        GridPane.setConstraints( l2, 0 ,2);GridPane.setConstraints( g2, 2 ,2);
        GridPane.setConstraints( l3, 0 ,3);GridPane.setConstraints( g3, 2 ,3); //made by pterekhov saratov russia
        GridPane.setConstraints( l4, 0 ,4);GridPane.setConstraints( g4, 2 ,4);
        GridPane.setConstraints( l5, 0 ,5);GridPane.setConstraints( g5, 2 ,5);
        GridPane.setConstraints( Result1, 0 ,6);GridPane.setConstraints( Result2, 2 ,6);
        GridPane.setConstraints( closeButton, 1 ,15);


        VBox layout = new VBox(40);


        closeButton.setOnAction(event -> {
            window.close();
        });

        gridPane.getChildren().addAll(
                lab1,lab2,l1,l2,l3,l4,l5,g1,g2,g3,g4,g5,closeButton,Result1,Result2
        );
        gridPane.setAlignment(Pos.TOP_CENTER);

        Scene scene1 = new Scene(gridPane);
        window.setResizable(false);
        window.setScene(scene1);
        window.show();
    }
}
