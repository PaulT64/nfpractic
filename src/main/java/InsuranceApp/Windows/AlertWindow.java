package InsuranceApp.Windows;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertWindow {
    public static void display(String title, String message, int choise){

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(330);
        window.setMinHeight(170);
        Label lab = new Label(message);
        lab.setFont(Font.font(20));
        VBox layout = new VBox(20);
        switch (choise) {
            case 1:
                Button closeButton = new Button("Ок");
                closeButton.setTextAlignment(TextAlignment.CENTER);
                closeButton.setMinSize(100,25);
                closeButton.setAlignment(Pos.BOTTOM_CENTER);
                closeButton.setOnAction(event -> {
                    window.close();
                });

                layout.getChildren().addAll(lab, closeButton);
                layout.setAlignment(Pos.CENTER);
                break;
            case 2:
                Button closeButton1 = new Button("Да");

                closeButton1.setTextAlignment(TextAlignment.CENTER);
                closeButton1.setMinSize(100,25);
                closeButton1.setAlignment(Pos.BOTTOM_CENTER);
                closeButton1.setOnAction(event -> {
                    System.exit(0);
                });

                Button closeButton2 = new Button("Нет");

                closeButton2.setTextAlignment(TextAlignment.CENTER);
                closeButton2.setMinSize(100,25);
                closeButton2.setAlignment(Pos.BOTTOM_CENTER);
                closeButton2.setOnAction(event -> {
                    window.close();
                });

                layout.getChildren().addAll(lab, closeButton1,closeButton2);
                layout.setAlignment(Pos.CENTER);
                break;
            default: break;

        }

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.show();
    }
}
