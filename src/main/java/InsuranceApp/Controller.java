package InsuranceApp;

import InsuranceApp.CalculationHandler.Handler;
import InsuranceApp.Windows.AlertWindow;
import InsuranceApp.Windows.ResultWindow;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.FileNotFoundException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField ContractSum;

    @FXML
    private TextField Percent;

    @FXML
    private TextField Premiya;

    @FXML
    private ChoiceBox ChoseGender;

    @FXML
    private DatePicker BirthDate;

    @FXML
    private Button StartButton;

    @FXML
    private Button ExitButton;

    @FXML
    private TextField cntN;


    @FXML
    void initialize() {
        Handler HND = new Handler("HANDLER");
        LocalDate CurrentDate = LocalDate.now();
        final LocalDate[] birthDate = new LocalDate[1];
        final int[] age = new int[1]; //возраст застрахованного
        int limitAge = 120; //максимально возможный возраст жизни
        final double[] percent = new double[1]; //процентная ставка
        final double[] contractSum = new double[1]; //сумма, на которую заключили договор
        final double[] premiya = new double[1]; //страховая премия
        final int[] CntN = new int[1];//срок договора
        final int[] gender = new int[1];
        int[] genders = new int[] {1,2};


        ChoseGender.setItems(FXCollections.observableArrayList(
                "Мужчина", "Женщина"
        ));

        ChoseGender.getSelectionModel().selectedIndexProperty().addListener(
                (ObservableValue<? extends Number> ov, Number old_value, Number new_value) ->{
                    gender[0] = genders[(int) new_value];
                }
        );

//процентная ставка\\
//======================================================================================================================================//
        Percent.lengthProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() > oldValue.intValue()) {
                    char ch = Percent.getText().charAt(oldValue.intValue());
                    // проверяем текст (числа ли?)
                    if (!(ch >= '0' && ch <= '9' )) {
                        // если не числа, то ставим предыдущий текст
                        Percent.setText(Percent.getText().substring(0,Percent.getText().length()-1));
                    }
                }
            }

        });
//======================================================================================================================================//

//страховая премия\\
//======================================================================================================================================//
        Premiya.lengthProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() > oldValue.intValue()) {
                    char ch = Premiya.getText().charAt(oldValue.intValue());
                    // проверяем текст (числа ли?)
                    if (!(ch >= '0' && ch <= '9' )) {
                        // если не числа, то ставим предыдущий текст
                        Premiya.setText(Premiya.getText().substring(0,Premiya.getText().length()-1));
                    }
                }
            }

        });
//======================================================================================================================================//

//сумма договора\\
//======================================================================================================================================//
        cntN.lengthProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() > oldValue.intValue()) {
                    char ch = cntN.getText().charAt(oldValue.intValue());
                    // проверяем текст (числа ли?)
                    if (!(ch >= '0' && ch <= '9' )) {
                        // если не числа, то ставим предыдущий текст
                        cntN.setText(cntN.getText().substring(0,cntN.getText().length()-1));
                    }
                }
            }

        });
//======================================================================================================================================//


//срок договора\\
//======================================================================================================================================//
        ContractSum.lengthProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (newValue.intValue() > oldValue.intValue()) {
                    char ch = ContractSum.getText().charAt(oldValue.intValue());
                    // проверяем текст (числа ли?)
                    if (!(ch >= '0' && ch <= '9' )) {
                        // если не числа, то ставим предыдущий текст
                        ContractSum.setText(ContractSum.getText().substring(0,ContractSum.getText().length()-1));
                    }
                }
            }

        });
//======================================================================================================================================//



//дата рождения\\
//======================================================================================================================================//
        BirthDate.setDayCellFactory(picker -> new DateCell() { //запрет выбора даты позже настоящего времени
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();

                setDisable(empty || date.compareTo(today) > 0 );
            }
        });
        BirthDate.setOnAction(event -> {
            birthDate[0] = BirthDate.getValue();
            age[0] = Period.between(birthDate[0],CurrentDate).getYears();
        });
//======================================================================================================================================//
        StartButton.setOnAction(event -> {

            if( Percent.getText() == null || Premiya.getText() == null || ContractSum.getText() == null || cntN.getText() == null || BirthDate.getValue() == null || gender == null)
                {
                AlertWindow.display("Внимание", "Пожалуйста заполните все поля",1);
            }else {
                try {
                    percent[0] = Double.parseDouble(Percent.getText());
                    Percent.setText(null);
                    premiya[0] = Double.parseDouble(Premiya.getText());
                    Premiya.setText(null);
                    contractSum[0] = Double.parseDouble(ContractSum.getText());
                    ContractSum.setText(null);
                    CntN[0] = Integer.parseInt(cntN.getText());
                    cntN.setText(null);
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }
                Map values = new HashMap<String,Double>();
                double chance;

                try {
                    HND.Start(age[0], limitAge, percent[0], contractSum[0], premiya[0], CntN[0], gender[0]);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                values = HND.getMap();
                chance = HND.getChance();
                ResultWindow.display("Результат",values,CntN[0], chance);
                percent[0] = 0.0f;
                premiya[0] = 0.0f;
                contractSum[0] = 0.0f;
                CntN[0] = (int) 0.0f;
            }
        });

        ExitButton.setOnAction(event -> {
            AlertWindow.display("Внимание", "Выйти из приложения ?", 2);
        });

    }
}
