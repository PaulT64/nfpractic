package InsuranceApp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("/Form1.fxml"));
        primaryStage.setTitle("InsHelper");
        primaryStage.setScene(new Scene(root, 400, 565));
        primaryStage.show();
        primaryStage.setResizable(false);

    }


    public static void main(String[] args) throws IOException {

        launch(args);

    }

}